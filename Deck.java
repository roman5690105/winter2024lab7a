import java.util.Random;

public class Deck{
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	
	
	//CONSTRUCTOR
	public Deck(){
		rng = new Random();
		this.numberOfCards = 52;
		this.cards = new Card[numberOfCards]; //KINDA IMPORTANT
		String[] value = {"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"};
		String[] suit = {"Hearts", "Spades", "Diamonds", "Clubs"};
		
		int counter = 0;
		
		for(int i = 0; i < suit.length; i++){
			for(int y = 0; y < value.length; y++){
				this.cards[counter] = new Card(suit[i], value[y]);
				counter++;
			}
		}
	}
	
	//LENGTH
	public int length(){
		return this.numberOfCards;
	}
	
	
	//DRAWTOP
	public Card drawTopCard(){
		this.numberOfCards--;
		return this.cards[numberOfCards];
	}
	
	
	//TOSTRING
	public String toString(){
		String output = "";
		for(int i = 0; i < numberOfCards; i++){
			output += this.cards[i] + "\n";
		}
		return output;
	}
	
	
	//SHUFFLE
	public void shuffle(){
		for(int i = 0; i < this.numberOfCards; i++){
			int rng_spot = rng.nextInt(i, this.numberOfCards);
			Card placeholder = cards[i];
			cards[i] = cards[rng_spot];
			cards[rng_spot] = placeholder;
		}
	}
	

	

}