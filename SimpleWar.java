public class SimpleWar{
	public static void main(String[] args){
		Deck warDeck = new Deck();
		warDeck.shuffle();
		
		int playerOneScore = 0;
		int playerTwoScore = 0;
		
		
		
		
		
		int roundCounter = 1;
		while(true){
			if(warDeck.length() == 0){
				break;
			}
			
			System.out.println("Round: " + roundCounter);
			System.out.println("");
			roundCounter ++;
			
			System.out.println("PlayerOne score: " + playerOneScore);
			System.out.println("PlayerTwo score: " + playerTwoScore);
			System.out.println("-----------------------");
			
			Card testCardOne = warDeck.drawTopCard();
			Card testCardTwo = warDeck.drawTopCard();
			
			double scoreOne = testCardOne.calculateScore();
			double scoreTwo = testCardTwo.calculateScore();
			
			System.out.println("PlayerOne drew: " + testCardOne);
			System.out.println("Worth: " + scoreOne);
			System.out.println("");
			System.out.println("PlayerTwo drew: " + testCardTwo);
			System.out.println("Worth: " + scoreTwo);
			
			System.out.println("-----------------------");
			
			double winCard = Math.max(scoreOne, scoreTwo);
			
			if(winCard == scoreOne){
				playerOneScore += 1;
				System.out.println("PlayerOne wins round with: " + testCardOne);
			}
			if(winCard == scoreTwo){
				playerTwoScore += 1;
				System.out.println("PlayerTwo wins round with: " + testCardTwo);
			}
			
			System.out.println("-----------------------");
			
			System.out.println("PlayerOne score: " + playerOneScore);
			System.out.println("PlayerTwo score: " + playerTwoScore);
			
			System.out.println("-----------------------");
			System.out.println("");
			System.out.println("");
			System.out.println("");
			System.out.println("");
			
			
		}
		int winner = Math.max(playerOneScore, playerTwoScore);
		
		if(playerOneScore == winner && playerTwoScore == winner){
			System.out.println("You tied!");
		}
		if(playerOneScore == winner){
			System.out.println("PlayerOne won with " + winner + " points");
		}
		if(playerTwoScore == winner){
			System.out.println("PlayerTwo won with " + winner + " points");
		}
	}
	
}