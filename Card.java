public class Card{
	
	//FIELDS
	private String suit;
	private String value;
	
	//CONSTRUCTOR
	public Card(String suit, String value){
		this.suit = suit;
		this.value = value;
	}
	
	//TOSTRING
	public String toString(){
		return value + " of " + suit;
	}

	//SCORE CALCULATOR
	public double calculateScore(){
		double score = 0.0;
		
		if(this.suit.equals("Hearts")){
			score += 0.4;
		}
		if(this.suit.equals("Spades")){
			score += 0.3;
		}
		if(this.suit.equals("Diamonds")){
			score += 0.2;
		}
		if(this.suit.equals("Clubs")){
			score += 0.1;
		}
		
		if(this.value.equals("Ace")){
			score += 1;
		}
		if(this.value.equals("Two")){
			score += 2;
		}
		if(this.value.equals("Three")){
			score += 3;
		}
		if(this.value.equals("Four")){
			score += 4;
		}
		if(this.value.equals("Five")){
			score += 5;
		}
		if(this.value.equals("Six")){
			score += 6;
		}
		if(this.value.equals("Seven")){
			score += 7;
		}
		if(this.value.equals("Eight")){
			score += 8;
		}
		if(this.value.equals("Nine")){
			score += 9;
		}
		if(this.value.equals("Ten")){
			score += 10;
		}
		if(this.value.equals("Jack")){
			score += 11;
		}
		if(this.value.equals("Queen")){
			score += 12;
		}
		if(this.value.equals("King")){
			score += 13;
		}
		
		return score;
	}

	//GETTERS
	public String getSuit(){
		return this.suit;
	}
	public String getValue(){
		return this.value;
	}
}